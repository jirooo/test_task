#!/usr/bin/env python
# coding: utf-8

# # Numpy part

# In[1]:


import numpy as np
import matplotlib.pyplot as plt


# In[2]:


np.random.seed(42)


# In[3]:


arr = np.array([0])
print(f'a)')


# In[4]:


for i in range(100):
    arr = np.append(arr, arr[-1]+(np.random.random()*2-1))

# len(arr)    
print('b)')


# In[5]:


plt.plot([i for i in range(len(arr))], arr)
plt.show()
print('c)')


# In[6]:


print(f'max = {max(arr)}, min = {min(arr)}\nd)')


# In[7]:


def func(threshold):
    arr = np.array([0])
    for i in range(100):
        arr = np.append(arr, arr[-1]+(np.random.random()*2-1))
#    print(f'max = {max(arr)}')
    if max(arr) > 30:
        plt.plot([i for i in range(len(arr))], arr)
        plt.show()
    return max(arr)


# In[8]:


arr_ = np.array([])

for i in range(50):
    arr_ = np.append(arr_, func(30))

print(f'Number of elements greater then 30: {sum(arr_>30)}')
plt.plot([i for i in range(arr_.shape[0])], arr_)
plt.show()
print('e)')