# # pandas

# In[9]:


import pandas as pd


# In[25]:


df = pd.DataFrame([['dolnośląskie','kujawsko-pomorskie','lubelskie','lubuskie','łódzkie','małopolskie','mazowieckie'],
              np.arange(2,16,2), 
              ['Wrocław','Bydgoszcz / Toruń','Lublin','Gorzów Wielkopolski / Zielona Góra2','Łódź','Kraków','Warszawa'],
              [19946.7,17971.34,25122.46,13987.93,18218.95,15182.79,35558.47],
              [2904198,2086210,2139726,1018084,2493603,3372618,5349114]],
              index=['województwo','nr rejestru','miasto wojewódzkie','powierzchnia [km**2]','ludność']).T


# In[26]:


df


# In[27]:


print('a)')
print(df.loc[df['powierzchnia [km**2]'] >= 20000]['miasto wojewódzkie'])


# In[28]:


new_df = df.loc[df['ludność']>2*10**6, 'województwo']
print(f'b)\n\n{new_df}')


# In[29]:


df.loc[df.shape[0]] = ['wielkopolskie', 30, 'Poznań', 29826.50, 3475323]
print('c)')


# In[30]:


df = df.sort_values(by='ludność', ascending=False)
print('d)')


# In[31]:


df = df[['nr rejestru', 'miasto wojewódzkie', 'powierzchnia [km**2]', 'ludność', 'województwo']]
print('e)')


# In[32]:


df['województwo'] = df['województwo'].str.capitalize()
print('f)')


# In[33]:


def custom_agg(df):
    ans = df['ludność']/df['powierzchnia [km**2]']
    return ans
    
ser_obj = df.groupby('województwo').apply(custom_agg)>140

print(f"g)\n\n{ser_obj}")


# In[34]:


df.drop(df[df['województwo'] == 'Lubuskie'].index.values, inplace=True)
print('h)')


# In[37]:


df['powierzchnia [km**2]'] = df['powierzchnia [km**2]'].astype(float)
df['ludność'] = df['ludność'].astype(float)
print('i)\n')
print(df.describe(include=[float]))


# In[ ]: